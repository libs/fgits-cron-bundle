<?php

namespace Fgits\Bundle\CronBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Fgits\Bundle\CronBundle\Entity\CronJob;
use Fgits\Bundle\CronBundle\Entity\CronJobResult;

/**
 * @extends ServiceEntityRepository<CronJobResult>
 *
 * @method CronJobResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method CronJobResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method CronJobResult[]    findAll()
 * @method CronJobResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CronJobResultRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, CronJobResult::class);
    }

    public function deleteOldLogs(CronJob $job = null, bool $flush = true): void
    {
        // Unfortunately, because we can't use DELETE k WHERE k.id > (SELECT MAX(k2.id) FROM k2)
        // we have to select the max IDs first
        $data = $this->getEntityManager()
            ->createQuery(
                'SELECT job.id, MAX(result.id) FROM '.CronJob::class.' job
                                                                  JOIN job.results result
                                                                  WHERE result.runAt < :date_offset
                                                                  GROUP BY result.job'
            )
            ->setParameter('date_offset', new \DateTimeImmutable('-4 days'))
            ->getResult();

        foreach ($data as $datum) {
            $jobId = $datum['id'];
            $minId = $datum[1];

            if (!$job || $job->getId() === $jobId) {
                $this->getEntityManager()->createQuery(
                    'DELETE '.CronJobResult::class.' result
                                                        WHERE result.id < :minId
                                                        AND result.job = :jobId'
                )
                    ->setParameter('minId', $minId)
                    ->setParameter('jobId', $jobId)
                    ->getResult();
            }
        }

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function save(CronJobResult $result, bool $flush = true): void
    {
        $this->getEntityManager()->persist($result);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
