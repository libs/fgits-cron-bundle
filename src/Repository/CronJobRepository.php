<?php

namespace Fgits\Bundle\CronBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Fgits\Bundle\CronBundle\Entity\CronJob;

/**
 * @extends ServiceEntityRepository<CronJob>
 *
 * @method CronJob|null find($id, $lockMode = null, $lockVersion = null)
 * @method CronJob|null findOneBy(array $criteria, array $orderBy = null)
 * @method CronJob[]    findAll()
 * @method CronJob[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CronJobRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry
    ) {
        parent::__construct($registry, CronJob::class);
    }

    /**
     * @return string[]
     */
    public function findCronjobNames(): array
    {
        $data = $this->createQueryBuilder('c')
            ->select('c.command')
            ->getQuery()
            ->getScalarResult();

        return array_values(array_map(fn ($item) => $item['command'], $data));
    }

    /**
     * @return CronJob[]
     *
     * @throws \Exception
     */
    public function findDueTasks(): array
    {
        $lockId      = uniqid();
        $lockTimeout = new \DateTimeImmutable(sprintf('-%s', CronJob::LOCK_TIMEOUT));
        $qb          = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->update(CronJob::class, 'c')
            ->set('c.lockId', $qb->expr()->literal($lockId))
            ->set('c.lock', $qb->expr()->literal(date('Y-m-d H:i:s')))
            ->where('c.nextRun <= :now')
            ->andWhere('c.enabled = true')
            ->andWhere('c.lock IS NULL OR c.lock < :lock_timeout')
            ->setParameter('now', new \DateTimeImmutable())
            ->setParameter('lock_timeout', $lockTimeout)
            ->getQuery()
            ->getResult();

        return $this->findBy(['lockId' => $lockId]);
    }

    public function save(CronJob $job, bool $flush = true): void
    {
        $this->getEntityManager()->persist($job);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function delete(CronJob $job, bool $flush = true): void
    {
        $this->getEntityManager()->remove($job);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
