<?php

namespace Fgits\Bundle\CronBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: 'Fgits\Bundle\CronBundle\Repository\CronJobRepository')]
class CronJob
{
    public const LOCK_TIMEOUT = '30min';

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    protected ?int $id = null;

    #[ORM\Column()]
    protected string $command;

    #[ORM\Column()]
    protected string $description;

    #[ORM\Column(name: 'job_interval', type: 'string', length: 40)]
    protected string $interval;

    #[ORM\Column(name: 'run_at', type: 'string', length: 40, nullable: true)]
    protected ?string $runAt = null;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $nextRun;

    #[ORM\Column(name: '`lock`', type: 'datetime_immutable', nullable: true)]
    protected ?\DateTimeImmutable $lock = null;

    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $lockId = null;

    #[ORM\Column(type: 'boolean')]
    protected bool $enabled;

    /**
     * @var Collection<int, CronJobResult> $results
     */
    #[ORM\OneToMany(mappedBy: 'job', targetEntity: CronJobResult::class, cascade: ['remove', 'persist'])]
    protected Collection $results;

    #[ORM\OneToOne(targetEntity: CronJobResult::class, cascade: ['ALL'])]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    protected ?CronJobResult $mostRecentRun = null;

    public function __construct()
    {
        $this->results = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommand(): string
    {
        return $this->command;
    }

    public function setCommand(string $command): static
    {
        $this->command = $command;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getInterval(): string
    {
        return $this->interval;
    }

    public function setInterval(string $interval): self
    {
        $this->interval = $interval;

        return $this;
    }

    public function getNextRun(): \DateTimeImmutable
    {
        return $this->nextRun;
    }

    public function setNextRun(\DateTimeImmutable $nextRun): self
    {
        $this->nextRun = $nextRun;

        return $this;
    }

    public function addCronJobResult(CronJobResult $result): self
    {
        $this->results->add($result);

        return $this;
    }

    /**
     * @return Collection<int, CronJobResult>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function getMostRecentRun(): ?CronJobResult
    {
        return $this->mostRecentRun;
    }

    public function setMostRecentRun(?CronJobResult $mostRecentRun): self
    {
        $this->mostRecentRun = $mostRecentRun;

        return $this;
    }

    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getRunAt(): ?string
    {
        return $this->runAt;
    }

    public function setRunAt(?string $runAt): self
    {
        $this->runAt = $runAt;

        return $this;
    }

    public function getLock(): ?\DateTimeImmutable
    {
        return $this->lock;
    }

    public function setLock(?\DateTimeImmutable $lock): self
    {
        $this->lock = $lock;

        return $this;
    }

    public function getLockId(): ?string
    {
        return $this->lockId;
    }

    public function setLockId(?string $lockId): self
    {
        $this->lockId = $lockId;

        return $this;
    }
}
