<?php

namespace Fgits\Bundle\CronBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: 'Fgits\Bundle\CronBundle\Repository\CronJobResultRepository')]
class CronJobResult
{
    public const SUCCEEDED = 'succeeded';
    public const FAILED    = 'failed';
    public const SKIPPED   = 'skipped';

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    protected ?int $id = null;

    #[ORM\Column(type: 'datetime_immutable')]
    protected ?\DateTimeImmutable $runAt = null;

    #[ORM\Column(type: 'float')]
    protected ?float $runTime = null;

    #[ORM\Column(type: 'string')]
    protected ?string $result = null;

    #[ORM\Column(type: 'text')]
    protected ?string $output = null;

    #[ORM\ManyToOne(targetEntity: CronJob::class, inversedBy: 'results')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    protected ?CronJob $job = null;

    public function __construct()
    {
        $this->runAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRunAt(): ?\DateTimeImmutable
    {
        return $this->runAt;
    }

    public function setRunAt(\DateTimeImmutable $runAt): self
    {
        $this->runAt = $runAt;

        return $this;
    }

    public function getRunTime(): ?float
    {
        return $this->runTime;
    }

    public function setRunTime(float $runTime): self
    {
        $this->runTime = $runTime;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function setResult(string $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getOutput(): ?string
    {
        return $this->output;
    }

    public function setOutput(string $output): self
    {
        $this->output = $output;

        return $this;
    }

    public function getJob(): ?CronJob
    {
        return $this->job;
    }

    public function setJob(CronJob $job): self
    {
        $this->job = $job;

        return $this;
    }
}
