<?php

namespace Fgits\Bundle\CronBundle\Command;

use Fgits\Bundle\CronBundle\Attribute\CronJob;
use Fgits\Bundle\CronBundle\Repository\CronJobRepository;
use Fgits\Bundle\CronBundle\Repository\CronJobResultRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[CronJob('PT24H')]
#[AsCommand('cron:pruneLogs', 'Prunes the logs for each cron job, leaving only recent failures and the most recent success')]
class CronPruneLogsCommand extends Command
{
    public function __construct(
        private readonly CronJobRepository $cronJobRepository,
        private readonly CronJobResultRepository $cronJobResultRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('job', InputArgument::OPTIONAL, 'Operate only on this job');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $jobName = $input->getArgument('job');

        $cronjob = $this->cronJobRepository->findOneBy(['command' => $jobName]);

        if ($jobName) {
            $output->writeln(sprintf('Pruning logs for cron job: %s', $jobName));
        } else {
            $output->writeln('Pruning logs for all cron jobs');
        }

        if ($jobName) {
            if (!$cronjob) {
                $output->writeln(sprintf('Couldn\'t find job with name: %s', $jobName));

                return Command::FAILURE;
            }

            $this->cronJobResultRepository->deleteOldLogs($cronjob);
        } else {
            $this->cronJobResultRepository->deleteOldLogs();
        }

        $output->writeln('Logs pruned successfully');

        return Command::SUCCESS;
    }
}
