<?php

namespace Fgits\Bundle\CronBundle\Command;

use Fgits\Bundle\CronBundle\Attribute\CronAttributeInterface;
use Fgits\Bundle\CronBundle\Attribute\CronJob as CronjobAttribute;
use Fgits\Bundle\CronBundle\Attribute\CronRunAt as CronRunAtAttribute;
use Fgits\Bundle\CronBundle\Entity\CronJob;
use Fgits\Bundle\CronBundle\Repository\CronJobRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LazyCommand;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('cron:scan', 'Scans for any new or deleted cron jobs')]
class CronScanCommand extends Command
{
    private OutputInterface $output;

    /**
     * CronScanCommand constructor.
     */
    public function __construct(
        private readonly CronJobRepository $cronJobRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('keep-deleted', 'k', InputOption::VALUE_NONE, 'If set, deleted cron jobs will not be removed')
            ->addOption('default-disabled', 'd', InputOption::VALUE_NONE, 'If set, new jobs will be disabled by default');
    }

    /**
     * @throws \ReflectionException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;

        $keepDeleted     = $input->getOption('keep-deleted');
        $defaultDisabled = $input->getOption('default-disabled');

        $knownJobs = $this->cronJobRepository->findCronjobNames();
        $knownJobs = array_fill_keys($knownJobs, true);

        // Enumerate all the jobs currently loaded

        $commands = $this->getApplication()?->all() ?? [];

        foreach ($commands as $command) {
            if ($command instanceof LazyCommand) {
                $command = $command->getCommand();
            }
            // Check for supported Attributes
            $refClass = new \ReflectionClass($command);

            $attributes = $refClass->getAttributes();

            foreach ($attributes as $attribute) {
                $realAttribute = $attribute->newInstance();

                if ($realAttribute instanceof CronAttributeInterface) {
                    $cronJobName = $command->getName() ?? '';

                    if (array_key_exists($cronJobName, $knownJobs)) {
                        // Clear it from the known jobs so that we don't try to delete it
                        unset($knownJobs[$cronJobName]);

                        $this->updateCronjob($command, $realAttribute);
                    } else {
                        $this->createCronjob($command, $realAttribute, $defaultDisabled);
                    }
                }
            }
        }

        // Clear any jobs that weren't found
        if (!$keepDeleted) {
            foreach (array_keys($knownJobs) as $deletedJob) {
                $this->output->writeln("Deleting job: $deletedJob");

                $jobToDelete = $this->cronJobRepository->findOneBy(['command' => $deletedJob]);

                if (!$jobToDelete) {
                    $this->output->writeln("Job $deletedJob not found in database");
                    continue;
                }

                $this->cronJobRepository->delete($jobToDelete);
            }
        }

        $this->output->writeln('Finished scanning for cron jobs');

        return Command::SUCCESS;
    }

    /**
     * Create new Cronjob with parameters of the attribute.
     *
     * @throws CommandNotFoundException
     */
    protected function createCronjob(Command $command, CronAttributeInterface $cronAttribute, bool $defaultDisabled = false): void
    {
        $newCronjob = new CronJob();

        if (!$command->getName()) {
            throw new CommandNotFoundException('Command name is required');
        }

        $newCronjob
            ->setCommand($command->getName())
            ->setDescription($command->getDescription())
            ->setEnabled(!$defaultDisabled);

        if ($cronAttribute instanceof CronjobAttribute) {
            $newCronjob
                ->setInterval($cronAttribute->value)
                ->setNextRun(new \DateTimeImmutable());
        } elseif ($cronAttribute instanceof CronRunAtAttribute) {
            try {
                $nextRun = new \DateTimeImmutable($cronAttribute->value);
            } catch (\Exception) {
                $this->output->writeln(sprintf('Error parsing time value in CronRunAt-Attribute for job %s', $command->getName()));

                return;
            }

            if ($nextRun <= new \DateTimeImmutable()) {
                $nextRun = $nextRun->add(new \DateInterval('P1D'));
            }

            $newCronjob
                ->setInterval('PT24H')
                ->setNextRun($nextRun)
                ->setRunAt($cronAttribute->value);
        }

        $this->cronJobRepository->save($newCronjob);

        $this->output->writeln(sprintf('Added cronjob %s with interval %s and run-at %s', $newCronjob->getCommand(), $newCronjob->getInterval(), $newCronjob->getRunAt()));
    }

    /**
     * Update given Cronjob wit parameters of the attribute.
     *
     * @throws \Exception
     */
    protected function updateCronjob(Command $command, CronAttributeInterface $cronAttribute): void
    {
        $cronJob = $this->cronJobRepository->findOneBy(['command' => $command->getName()]);

        if (!$cronJob) {
            return;
        }

        $cronJob->setDescription($command->getDescription());

        if ($cronAttribute instanceof CronjobAttribute) {
            if ($cronJob->getInterval() !== $cronAttribute->value) {
                $nextRun = new \DateTimeImmutable();
                $nextRun = $nextRun->add(new \DateInterval($cronAttribute->value));

                $cronJob
                    ->setInterval($cronAttribute->value)
                    ->setRunAt(null)
                    ->setNextRun($nextRun);
            }
        } elseif ($cronAttribute instanceof CronRunAtAttribute) {
            if ($cronJob->getRunAt() !== $cronAttribute->value) {
                try {
                    $nextRun = new \DateTimeImmutable($cronAttribute->value);
                } catch (\Exception) {
                    $this->output->writeln(sprintf('Error parsing time value in CronRunAt-Attribute for job %s', $command->getName()));

                    return;
                }

                if ($nextRun <= new \DateTimeImmutable()) {
                    $nextRun = $nextRun->add(new \DateInterval('P1D'));
                }

                $cronJob
                    ->setInterval('PT24H')
                    ->setRunAt($cronAttribute->value)
                    ->setNextRun($nextRun);
            }
        }

        $this->cronJobRepository->save($cronJob);

        $this->output->writeln(sprintf('Updated cronjob %s with interval %s and run-at %s', $cronJob->getCommand(), $cronJob->getInterval(), $cronJob->getRunAt()));
    }
}
