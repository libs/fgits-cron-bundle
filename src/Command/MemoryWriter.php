<?php

namespace Fgits\Bundle\CronBundle\Command;

use Symfony\Component\Console\Output\Output;

/**
 * MemoryWriter implements OutputInterface, writing to an internal buffer.
 */
class MemoryWriter extends Output
{
    protected string $backingStore = '';

    public function __construct($verbosity = self::VERBOSITY_NORMAL)
    {
        parent::__construct($verbosity);
    }

    public function doWrite(string $message, bool $newline): void
    {
        $this->backingStore .= $message;
        if ($newline) {
            $this->backingStore .= "\n";
        }
    }

    public function getOutput(): string
    {
        return $this->backingStore;
    }

    public function clear(): void
    {
        $this->backingStore = '';
    }
}
