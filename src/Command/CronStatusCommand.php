<?php

namespace Fgits\Bundle\CronBundle\Command;

use Fgits\Bundle\CronBundle\Entity\CronJob;
use Fgits\Bundle\CronBundle\Entity\CronJobResult;
use Fgits\Bundle\CronBundle\Repository\CronJobRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('cron:status', 'Displays the current status of cron jobs')]
class CronStatusCommand extends Command
{
    public function __construct(
        private readonly CronJobRepository $cronJobRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('job', InputArgument::OPTIONAL, 'Show information for only this job');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $jobName = $input->getArgument('job');
        $cronjob = $this->cronJobRepository->findOneBy(['command' => $jobName]);

        if (!$cronjob instanceof CronJob) {
            $output->writeln("Couldn't find a job by the name of $jobName");

            return Command::FAILURE;
        }

        $output->writeln('Cronjob status report:');

        $cronJobs = $jobName ? [$cronjob] : $this->cronJobRepository->findAll();

        foreach ($cronJobs as $cronJob) {
            $output->write(' - '.$cronJob->getCommand());

            if (!$cronJob->getEnabled()) {
                $output->write(' (disabled)');
            }

            $output->writeln('');
            $output->writeln('   Description: '.$cronJob->getDescription());

            if (!$cronJob->getEnabled()) {
                $output->writeln('   Not scheduled');
            } else {
                $output->write('   Scheduled for: ');

                $now = new \DateTimeImmutable();

                if (!$cronJob->getNextRun() || $cronJob->getNextRun() <= $now) {
                    $output->writeln('Next run');
                } else {
                    $output->writeln($cronJob->getNextRun()->format('%c'));
                }
            }
            if ($cronJob->getMostRecentRun()) {
                $status = 'Unknown';

                switch ($cronJob->getMostRecentRun()->getResult()) {
                    case CronJobResult::SUCCEEDED:
                        $status = 'Successful';
                        break;
                    case CronJobResult::SKIPPED:
                        $status = 'Skipped';
                        break;
                    case CronJobResult::FAILED:
                        $status = 'Failed';
                        break;
                }

                $output->writeln("   Status of last run: $status");
            } else {
                $output->writeln('   This job has not yet been run');
            }

            $output->writeln('');
        }

        return Command::SUCCESS;
    }
}
