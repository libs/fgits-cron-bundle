<?php

namespace Fgits\Bundle\CronBundle\Command;

use Fgits\Bundle\CronBundle\Entity\CronJob;
use Fgits\Bundle\CronBundle\Entity\CronJobResult;
use Fgits\Bundle\CronBundle\Event\CronJobFailedEvent;
use Fgits\Bundle\CronBundle\Event\CronJobRunningEvent;
use Fgits\Bundle\CronBundle\Event\CronJobSkippedEvent;
use Fgits\Bundle\CronBundle\Repository\CronJobRepository;
use Fgits\Bundle\CronBundle\Repository\CronJobResultRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

#[AsCommand('cron:run', 'Runs any scheduled cron jobs')]
class CronRunCommand extends Command
{
    protected OutputInterface $output;

    public function __construct(
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly CronJobRepository $cronJobRepository,
        private readonly CronJobResultRepository $cronJobResultRepository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('cron:run')
            ->addArgument('job', InputArgument::OPTIONAL, 'Run only specific job (if enabled)');
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;

        $start = microtime(true);

        if ($jobName = $input->getArgument('job')) {
            $cronjob = $this->cronJobRepository->findOneBy(['command' => $jobName]);

            if ($cronjob instanceof CronJob && $cronjob->getEnabled()) {
                $jobsToRun = [$cronjob];
            } else {
                $this->output->writeln("Couldn't find a job by the name of $jobName");

                return Command::FAILURE;
            }
        } else {
            $jobsToRun = $this->cronJobRepository->findDueTasks();
        }

        $this->output->writeln(sprintf('Running %s jobs', count($jobsToRun)));

        foreach ($jobsToRun as $job) {
            $this->runJob($job);
        }

        $end      = microtime(true);
        $duration = sprintf('%0.2f', $end - $start);

        $this->output->writeln("Cron run completed in $duration seconds");

        return Command::SUCCESS;
    }

    /**
     * Run a specific Cronjob.
     *
     * @throws \Exception
     */
    protected function runJob(CronJob $cronJob): void
    {
        $this->output->write(sprintf('Running Cronjob %s', $cronJob->getCommand()));

        $this->eventDispatcher->dispatch(new CronJobRunningEvent($cronJob));

        try {
            $commandToRun = $this->getApplication()?->get($cronJob->getCommand());

            if (!$commandToRun) {
                throw new CommandNotFoundException(sprintf('Command "%s" does not exist.', $cronJob->getCommand()));
            }
        } catch (\Throwable) {
            $this->output->writeln(' skipped (Command does not exist)');
            $result = $this->recordJobResult($cronJob, 0, 'Command does not exists', CronJobResult::SKIPPED);

            $this->eventDispatcher->dispatch(new CronJobFailedEvent($cronJob, $result, CronJobResult::SKIPPED));

            return;
        }

        $jobOutput = new MemoryWriter();
        $input     = new ArrayInput([]);
        $jobStart  = microtime(true);

        try {
            $returnCode = $commandToRun->run($input, $jobOutput);
        } catch (\Throwable $e) {
            $returnCode = CronJobResult::FAILED;

            $jobOutput->writeln('');
            $jobOutput->writeln(sprintf('Execution failed with Error: %s', get_class($e)));
            $jobOutput->writeln($e->__toString());
        }

        $jobEnd   = microtime(true);
        $duration = $jobEnd - $jobStart;

        // Clamp the result to accepted values
        if (!in_array($returnCode, [Command::SUCCESS, Command::FAILURE, Command::INVALID], true)) {
            $returnCode = CronJobResult::FAILED;
        } else {
            $returnCode = match ($returnCode) {
                Command::SUCCESS => CronJobResult::SUCCEEDED,
                Command::INVALID => CronJobResult::SKIPPED,
                default          => CronJobResult::FAILED,
            };
        }

        $this->output->writeln(sprintf('Status: %s after %s seconds', $returnCode, sprintf('%0.2f', $duration)));
        $result = $this->recordJobResult($cronJob, $duration, $jobOutput->getOutput(), $returnCode);

        switch ($returnCode) {
            case 0:
            case CronJobResult::SUCCEEDED:
                $this->eventDispatcher->dispatch(new CronJobRunningEvent($cronJob, $result, $returnCode));
                break;
            case CronJobResult::FAILED:
                $this->eventDispatcher->dispatch(new CronJobFailedEvent($cronJob, $result, $returnCode));
                break;
            case CronJobResult::SKIPPED:
                $this->eventDispatcher->dispatch(new CronJobSkippedEvent($cronJob, $result, $returnCode));
                break;
        }

        // And update the job with its next scheduled time
        if (null !== $cronJob->getRunAt()) {
            try {
                $nextRun = new \DateTimeImmutable($cronJob->getRunAt());
            } catch (\Exception) {
                $this->output->writeln(sprintf('Error parsing time value in CronRunAt-Attribute for job %s', $cronJob->getCommand()));

                $nextRun = $cronJob->getNextRun()->add(new \DateInterval('P1D'));
            }

            if ($nextRun <= new \DateTimeImmutable()) {
                $nextRun = $nextRun->add(new \DateInterval('P1D'));
            }
        } else {
            $nextRun = new \DateTimeImmutable();
            $nextRun = $nextRun->add(new \DateInterval($cronJob->getInterval()));
        }

        $cronJob
            ->setNextRun($nextRun)
            ->setLockId(null)
            ->setLock(null);

        $this->cronJobRepository->save($cronJob);
    }

    /**
     * Record the result of a CronJob.
     */
    protected function recordJobResult(CronJob $job, float $timeTaken, string $output, string $resultCode): CronJobResult
    {
        $result = new CronJobResult();

        $result
            ->setJob($job)
            ->setRunTime($timeTaken)
            ->setOutput($output)
            ->setResult($resultCode);

        $job->setMostRecentRun($result);

        $this->cronJobResultRepository->save($result);

        return $result;
    }
}
