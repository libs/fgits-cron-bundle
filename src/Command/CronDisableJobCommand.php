<?php

namespace Fgits\Bundle\CronBundle\Command;

use Fgits\Bundle\CronBundle\Repository\CronJobRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('cron:disable-job', 'Disables a cron job')]
class CronDisableJobCommand extends Command
{
    public function __construct(
        private readonly CronJobRepository $cronJobRepository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('job', InputArgument::REQUIRED, 'Name of the job to disable');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $jobName = $input->getArgument('job');
        $job     = $this->cronJobRepository->findOneBy(['command' => $jobName]);

        if (!$job) {
            $output->writeln(sprintf('Couldn\'t find job by name: %s', $jobName));

            return Command::FAILURE;
        }

        $job->setEnabled(false);

        $this->cronJobRepository->save($job);

        $output->writeln(sprintf('Disabled cron job with name: %s', $jobName));

        return Command::SUCCESS;
    }
}
