<?php

namespace Fgits\Bundle\CronBundle\Event;

use Fgits\Bundle\CronBundle\Entity\CronJob;
use Fgits\Bundle\CronBundle\Entity\CronJobResult;

abstract class AbstractCronJobEvent
{
    public function __construct(
        private CronJob $cronjob,
        private ?CronJobResult $cronjobResult = null,
        private ?string $result = null
    ) {
    }

    public function getCronjob(): CronJob
    {
        return $this->cronjob;
    }

    public function setCronjob(CronJob $cronjob): self
    {
        $this->cronjob = $cronjob;

        return $this;
    }

    public function getCronjobResult(): ?CronJobResult
    {
        return $this->cronjobResult;
    }

    public function setCronjobResult(CronJobResult $cronjobResult): self
    {
        $this->cronjobResult = $cronjobResult;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function setResult(string $result): self
    {
        $this->result = $result;

        return $this;
    }
}
