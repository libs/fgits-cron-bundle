<?php
/**
 * CronBundle CronJobRunningEvent.php.
 *
 * @author    Fabian Golle <fabian@golle-it.de>
 * @copyright Fabian Golle <fabian@golle-it.de>
 */

namespace Fgits\Bundle\CronBundle\Event;

class CronJobRunningEvent extends AbstractCronJobEvent
{
}
