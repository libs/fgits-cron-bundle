<?php

namespace Fgits\Bundle\CronBundle\Attribute;

#[\Attribute(\Attribute::TARGET_CLASS)]
class CronRunAt implements CronAttributeInterface
{
    public function __construct(
        public string $value,
    ) {
    }
}
