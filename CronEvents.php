<?php

namespace Fgits\Bundle\CronBundle;

final class CronEvents
{
    public const CRONJOB_ADDED     = 'coloursteam.cronjob.created';
    public const CRONJOB_RUNNING   = 'coloursteam.cronjob.running';
    public const CRONJOB_SUCCEEDED = 'coloursteam.cronjob.succeeded';
    public const CRONJOB_FAILED    = 'coloursteam.cronjob.failed';
    public const CRONJOB_SKIPPED   = 'coloursteam.cronjob.skipped';
}
